# Bookshelf API

Aplikasi backend sederhana yang menyediakan API untuk menambah, mengubah, menampilkan dan menghapus data buku. Aplikasi dibangun menggunakan Node JS dengan express library.
Data tersimpan dalam database PostgreSQL dengan menggunakan TypeORM untuk terhubung dengan Express. 

Service dilengkapi dengan Redis yang digunakan untuk melakukan cache terhadap endpoint dengan mode Get yang mengembalikan List of Data.

Source : Membuat Aplikasi Back-End untuk Pemula](https://www.dicoding.com/academies/261)

## Technology

- Node JS 16.17.0
- npm 8.15.0

### Installation

```bash
$ npm install
```

## Running the app

```bash
$ docker-compose up -d    # running postgresql database and redis

$ cp sample.env .env #  Copy .env-example and modify the .env file
$ cp sample.database.env .database.env #  Copy and modify

$ npm run start   # make sure you have installed postgres and redis

# production mode
$ npm run start:prod

```

## API Endpoint

## Auth

#### Login
Login to get JWT token with `username` and `password` as request body.

```http
POST /auth/
```


## Books 

#### Get all books
Get all data from book. Optional request can be add with offset to indicate index data in db and limit to indicate count of data as result.
```http
GET /books/
GET /books/?offset=0&&limit=2
```

#### Get book 
Get specific data from book by id
```http
GET /books/${id}
```

#### Add book
Create book data to the system. 
```http
POST /books/
```
#### Parameter
| Key     |  Description                       |
| :-------- | :-------------------------------- |
| `name` |  name of the book |
| `year` |   year of the book publisher |
| `author` | author of the book |
| `summary` | summary of the book |
| `publisher` | publisher of the book |
| `pageCount` | number indicates how many page in the book |

#### PATCH book 
Update book data with specific id. The request body has same structure with the POST method.
```http
PATCH /books/${id}
```

#### DELETE book 
Delete book data with specific id.
```http
DELETE /books/${id}
```

## User

#### Get all User
Get all data from user. Optional request can be add with offset to indicate index data in db and limit to indicate count of data as result.
```http
GET /user/
GET /user/?offset=3
GET /user/?limit=6
GET /user/?offset=3&&limit=6
```

#### Get all User Detail
Get all data from user with List book assosiate with data user. Optional request can be add with offset to indicate index data in db and limit to indicate count of data as result.
```http
GET /user/detail
GET /user/detail?offset=3
GET /user/detail?limit=6
GET /user/detail?offset=3&&limit=6
```

#### Get User
Get specific data from database by username
```http
GET /user/${username}
```

#### Get User
Get specific data with list book that assosiate with user from database by username
```http
GET /books/${username}/detail
```

#### Add User
Create user data to the system.
```http
POST /user/
```
#### Parameter
| Key       |
| :--------- |
| `name` |
| `username` |
| `password` |
| `email` |

#### PATCH User 
Update user data with specific username. The request body has same structure with the POST method.
```http
PATCH /user/${username}
```

#### DELETE User 
Delete user data with specific username.
```http
DELETE /user/${username}
```


## User Book

#### Get all User Book
Get all data from user book that defines data that has relation between user and book. Optional request can be add with offset to indicate index data in db and limit to indicate count of data as result.
```http
GET /user-book/
GET /user-book/?offset=3
GET /user-book/?limit=6
GET /user-book/?offset=3&&limit=6
```

#### Get User Book
Get specific data from database by userBookId
```http
GET /user-data/${userBookId}
```

#### Add User Book
Create user book data.
```http
POST /user-book/
```
#### Parameter
| Key       |  Description |
| :--------- | :------------- |
| `bookId` | id of book |
| `username` | username |
| `readPage` | define page being read |
| `finished` | define status of finished read |
| `reading` | defines status of reading book|


#### PATCH User Book
Update user book data with specific userBookId. The request body has same structure with the POST method.
```http
PATCH /user-book/${userBookId}
```

#### DELETE User Book
Delete user book data with specific userBookId.
```http
DELETE /user-book/${userBookId}
```


for details, please look into this file using Postman Collection `Bookshelf-Express.postman_collection.json` with adjustments to the environment


