import { createClient } from 'redis';
import env from '../../config/env';

let redisClient;

(async () => {
  redisClient = createClient({
    socket:{
        host:env.REDIS_HOST,
        port:env.REDIS_PORT,
    },

  });

  redisClient.on('error',(err)=>{
    console.log('Redis Client Error', err)
  });
  
  await redisClient.connect();

  redisClient.setData = async function(key,message){
    await this.set(key,message,{
        'EX':env.REDIS_EXPIRE
    });
  }
})();

export default redisClient;