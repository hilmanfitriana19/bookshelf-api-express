import { logger } from "../logger/logger.config";
import { userService } from "../service/user.service"
import { ResponseMessage } from "../utils/constant/response-message.constant";
import { responseUtil } from "../utils/response/response-utils";

export const AuthController = ({
    login : async (req,res) => {
        // logger.info(`Login Attempt Occur`);
        logger.info(`Login Attempt Occur`);
        const body = req.body;
        try{
            const result = await userService.login(body.username,body.password);
            if(result){
                logger.info("Login Attempt Success");
                return responseUtil.SuccessProcess(res,result);
            }else{
                logger.error("Login Attempt Failed");
                return responseUtil.UnauthorizedAccess(res,ResponseMessage.LoginFailed);
            }
        }catch(e){
            logger.error(`Login Error : ${e}`);
            return responseUtil.FailProcess(res);
        }

    }
})