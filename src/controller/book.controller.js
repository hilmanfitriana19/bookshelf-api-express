import redisClient from "../caching/redis/redis";
import { logger } from "../logger/logger.config";
import { bookService } from "../service/book.service";
import { ResponseMessage } from "../utils/constant/response-message.constant";
import { responseUtil } from "../utils/response/response-utils";
import { bookValidator } from "../validator/book-validator";
import { queryValidator } from "../validator/query-validator";


 export const BookController = ({
    create : async (req,res) =>{

        logger.info("Request Create Book");

        const body =  req.body;
        const validationResult = bookValidator.create.validate(body,{allowUnknown:true});
        if (validationResult.error) {
            logger.error(`Fail Create Book : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res,validationResult.error.message);
        }

        try{
            await bookService.create(body);
            responseUtil.SuccessCreated(res);
        }catch(e){
            
            if (e?.detail?.endsWith('exists.')){
                logger.error(`Create Book Error : ${e} - ${e?.detail}`);
                return responseUtil.FailProcess(res, ResponseMessage.NameBookTaken);
            }
            logger.error(`Create Book Error : ${e}`);
            return responseUtil.FailProcess(res, ResponseMessage.FailCreated);
            
        }
    },

    getAll : async (req,res) =>{
        logger.info(`Request Get List Book`);

        const offset = req.query.offset;
        const limit = req.query.limit;
        
        const validationResult = queryValidator.validate({offset,limit},{allowUnknown:true})
        if(validationResult.error){
            logger.error(`Fail Get List Book : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res,validationResult.error.message);
        }
        
        const cacheResults = await redisClient.get('BookList_'+offset+'_'+limit);
        if(cacheResults){
            logger.info(`Request Get List Book Success From Redis`);
            return res.json(JSON.parse(cacheResults));
        }else{
            logger.info(`Request Get List Book Success From DB`);
            const result = await bookService.getAll(offset,limit);
            redisClient.setData('BookList_'+offset+'_'+limit,JSON.stringify(result));
            return res.json(result);
        }
    },

    getOne : async (req,res) =>{
        logger.info(`Request Get Book With Specific Id`)

        const id =  req.params.id;
        const result = await bookService.getOne(id); 
        if(result) return res.json(result);

        return responseUtil.NotFoundData(res);
    },

    update : async (req,res) =>{

        logger.info(`Request Update Book`)

        const body =  req.body;
        const id = req.params.id;

        const validationResult = bookValidator.update.validate(body,{allowUnknown:true});
        if (validationResult.error) {
            logger.error(`Request Update Book Failed : ${validationResult.error.message}`)
            return responseUtil.FailProcess(res,validationResult.error.message);
        }

        if(! await bookService.getOne(id)){
            logger.error(`Request Update Book Failed : Book ${id} not Found`)
            return responseUtil.NotFoundData(res);
        }
        
        try{
            await bookService.update(id,body);
            return responseUtil.SuccessProcess(res,ResponseMessage.SuccessUpdated);
        } catch(e){
            logger.error(`Request Update Book Failed : ${e}`)
            if (e?.detail?.endsWith('exists.'))
                return responseUtil.FailProcess(res, ResponseMessage.NameBookTaken);
            return responseUtil.FailProcess(res,ResponseMessage.FailUpdated);
        }
    },

    delete : async (req,res) =>{

        logger.info(`Request Delete Book`)

        const id = req.params.id;

        if(! await bookService.getOne(id)){
            logger.error(`Request Delete Book Failed : Book ${id} not Found`);
            return responseUtil.NotFoundData(res);
        }

        try{
            const result = await bookService.delete(id);
            if (result.affected > 0)
                return responseUtil.SuccessProcess(res,ResponseMessage.SuccessDeleted);
            
            return responseUtil.SuccessProcess(res,ResponseMessage.FailDeleted);
        }catch(e){
            logger.error(`Request Delete Book Failed : ${e}`)
            return responseUtil.SuccessProcess(res,ResponseMessage.FailDeleted);
        }
    },
})