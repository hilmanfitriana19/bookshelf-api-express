import { logger } from "../logger/logger.config";
import { userBookService } from "../service/user-book.service";
import { ResponseMessage } from "../utils/constant/response-message.constant";
import { responseUtil } from "../utils/response/response-utils";
import { queryValidator } from "../validator/query-validator";

export const UserBookController = ({
    create : async (req,res) =>{

        logger.info(`Request Create User Book`);

        const body =  req.body;
        try{
            await userBookService.create(body);
            responseUtil.SuccessCreated(res);
        }catch(e){
            logger.error(`Request Create Book Failed : ${e}`)
            if (e?.detail?.endsWith('exists.'))
                return responseUtil.FailProcess(res, ResponseMessage.NameuserTaken);

            return responseUtil.FailProcess(res, ResponseMessage.FailCreated);
        }
    },

    getAll : async (req,res) =>{

        logger.info(`Request GET List User Book`)

        const offset = req.query.offset;
        const limit = req.query.limit;

        const validationResult = queryValidator.validate({offset,limit},{allowUnknown:true})
        if(validationResult.error){
            logger.error(`Request List User Book Failed ${validationResult.error.message}`)
            return responseUtil.FailProcess(res,validationResult.error.message);
        }

        const result = await userBookService.getAll(offset,limit);
        return res.json(result);
    },

    getOne : async (req,res) =>{
        logger.info(`Request Get User Book`)
        const userBookId =  req.params.userBookId;
        const result = await userBookService.getOne(userBookId); 
        if(result) return res.json(result);

        return responseUtil.NotFoundData(res);
    },

    update : async (req,res) =>{

        logger.info(`Request Update User Book`)

        const body =  req.body;
        const userBookId = req.params.userBookId;

        if(! await userBookService.getOne(userBookId)){
            logger.error(`Request Update User Book Failed : ${userBookId} not found`)
            return responseUtil.NotFoundData(res);
        }
        
        try{
            await userBookService.update(userBookId,body);
            return responseUtil.SuccessProcess(res,ResponseMessage.SuccessUpdated);
        } catch(e){
            logger.error(`Request Update User Book Failed : ${e}`)
            return responseUtil.FailProcess(res,ResponseMessage.FailUpdated);
        }
    },

    delete : async (req,res) =>{

        logger.info(`Request Delete User Book`)

        const userBookId = req.params.userBookId;

        if(! await userBookService.getOne(userBookId)){
            logger.error(`Request Delete User Book Failed : ${userBookId} not found`)
            return responseUtil.NotFoundData(res);
        }

        try{
            const result = await userBookService.delete(userBookId);
            if (result.affected > 0)
                return responseUtil.SuccessProcess(res,ResponseMessage.SuccessDeleted);
            
            return responseUtil.SuccessProcess(res,ResponseMessage.FailDeleted);
        }catch(e){
            logger.error(`Request Delete User Book Failed : ${e}`)
            return responseUtil.SuccessProcess(res,ResponseMessage.FailDeleted);
        }
    }, 
})