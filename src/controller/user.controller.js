import redisClient from "../caching/redis/redis";
import { logger } from "../logger/logger.config";
import { userService } from "../service/user.service";
import { ResponseMessage } from "../utils/constant/response-message.constant";
import { UserDetailResponseInterceptor } from "../utils/interceptor/user-response.interceptor";
import { responseUtil } from "../utils/response/response-utils";
import { queryValidator } from "../validator/query-validator";
import { userValidator } from "../validator/user-validator";

export const UserController = ({
    create : async (req,res) =>{

        logger.info(`Request Create User Data`);

        const body =  req.body;

        const validationResult = userValidator.create.validate(body,{allowUnknown:true});
        if (validationResult.error) {
            logger.error(`Request Create User Data Failed ${validationResult.error.message}`);
            return responseUtil.FailProcess(res,validationResult.error.message);
        }
        // di typeorm, ada bugs yang jika pk nya bukan int, maka fungsi create bisa jadi update,
        // maka dalam create sebelum dibuat di check manual dulu
        if(await userService.getOne(body?.username)){
            logger.error(`Request Create User Data Failed : ${ResponseMessage.UsernameTaken}`);
            return responseUtil.FailProcess(res, ResponseMessage.UsernameTaken);
        }
        try{
            await userService.create(body);
            responseUtil.SuccessCreated(res);
        }catch(e){
            logger.error(`Request Create User Data Failed : ${e}`);
            if (e?.detail?.endsWith('exists.'))
                return responseUtil.FailProcess(res, ResponseMessage.EmailTaken);

            return responseUtil.FailProcess(res, ResponseMessage.FailCreated);
        }
    },

    getAll : async (req,res) =>{
        logger.info(`Request Get List User Data`);

        const offset = req.query.offset;
        const limit = req.query.limit;

        const validationResult = queryValidator.validate({offset,limit},{allowUnknown:true})
        if(validationResult.error){
            logger.error(`Request Create User Data Failed : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res,validationResult.error.message);
        }

        const result = await userService.getAll(offset,limit);
        return res.json(result);
    },

    getAllDetail : async (req,res) =>{

        logger.info(`Request Get List User Data Detail`);

        const offset = req.query.offset;
        const limit = req.query.limit;

        const validationResult = queryValidator.validate({offset,limit},{allowUnknown:true})
        if(validationResult.error){
            logger.error(`Request Get List User Data Detail Failed : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res,validationResult.error.message);
        }

        const cacheResults = await redisClient.get('userListDetail_'+offset+'_'+limit);
        if(cacheResults){
            logger.info(`Request Get List User Data Detail from redis`);
            return res.json(JSON.parse(cacheResults));
        }else{
            logger.info(`Request Get List User Data Detail from DB`);
            const result = UserDetailResponseInterceptor(await userService.getAllDetail(offset,limit));
            redisClient.setData('userListDetail_'+offset+'_'+limit,JSON.stringify(result));
            return res.json(result);
        }

    },

    getOne : async (req,res) =>{

        logger.info(`Request Get User Data`);

        const username =  req.params.username;
        const result = await userService.getOne(username); 
        if(result) return res.json(result);

        return responseUtil.NotFoundData(res);
    },

    getOneDetail: async(req,res) => {

        logger.info(`Request Get User Data Detail`);

        const username =  req.params.username;
        const result = await userService.getOneDetail(username); 
        if(result) return res.json(UserDetailResponseInterceptor(result));
    },

    update : async (req,res) =>{

        logger.info(`Request Update User Data`);

        const body =  req.body;

        const validationResult = userValidator.update.validate(body,{allowUnknown:true});
        if (validationResult.error) {
            logger.error(`Request Update User Data Failed : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res,validationResult.error.message);
        }

        const username = req.params.username;

        if(! await userService.getOne(username)){
            logger.error(`Request Update User Data Failed : ${username} not found`);
            return responseUtil.NotFoundData(res);
        }
        
        try{
            await userService.update(username,body);
            return responseUtil.SuccessProcess(res,ResponseMessage.SuccessUpdated);
        } catch(e){
            logger.error(`Request Update User Data Failed : ${e}`);
            if (e?.detail?.endsWith('exists.'))
                return responseUtil.FailProcess(res, ResponseMessage.NameuserTaken);
            return responseUtil.FailProcess(res,ResponseMessage.FailUpdated);
        }
    },

    delete : async (req,res) =>{

        logger.info(`Request Delete User Data`);

        const username = req.params.username;

        if(! await userService.getOne(username)){
            logger.error(`Request Delete User Data Failed : ${username} not found`);
            return responseUtil.NotFoundData(res);
        }

        try{
            const result = await userService.delete(username);
            if (result.affected > 0)
                return responseUtil.SuccessProcess(res,ResponseMessage.SuccessDeleted);
            
            return responseUtil.SuccessProcess(res,ResponseMessage.FailDeleted);
        }catch(e){
            logger.error(`Request Delete User Data Failed : ${e}`);
            return responseUtil.SuccessProcess(res,ResponseMessage.FailDeleted);
        }
    }, 
})