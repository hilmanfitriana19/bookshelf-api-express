import { DataSource } from 'typeorm';
import UserSchema from '../entity/user-entity';
import env from '../config/env';
import UserBookSchema from '../entity/user-book-entity';
import BookSchema from '../entity/book-entitiy';

export const dataSource = new DataSource({
    type: env.DATABASE_TYPE,
    host: env.DATABASE_HOST,
    port: env.DATABASE_PORT,
    username: env.DATABASE_USER,
    password: env.DATABASE_PASSWORD,
    database: env.DATABASE_NAME,
    synchronize: true,
    entities:[UserBookSchema ,UserSchema, BookSchema],
})

export const connectDB = () => {
    dataSource.initialize().then(()=>{
        console.log('Data source has been Initialized');
    }).catch((e) => {
        console.error('Data Source initialize error', e);
    })
}

export const stopConnection = () =>{
    dataSource.destroy();
}
