import { EntitySchema } from "typeorm";
import { Book } from "../model/Book";

const BookSchema = new EntitySchema({
    name:'Book',
    target:Book,
    columns:{
        id:{
            primary:true,
            type:'int',
            generated: true
        },
        name:{
            type: 'varchar',
            length:50,
            unique:true, 
        },
        year:{
            type:'int'
            },
        author:{
            type:'varchar',
            default:''
        },
        summary:{
            type:'varchar',
            default:''
        },
        publisher:{
            type:'varchar',
            default:''
        },
        pageCount:{
            type:'int'
        },
        createdAt:{
            type:'timestamp without time zone',
            createDate:true,
            select:false
        },
        updatedAt:{
            type:'timestamp without time zone',
            updateDate:true,
            select:false
        },
    },
    relations:{
        user_book:{
            type:'one-to-many',
            target:"user_book",
            cascade:true,
            inverseSide:'book'
        }
    }
})

export default BookSchema;