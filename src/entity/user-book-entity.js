import { EntitySchema } from "typeorm";
import {UserBook} from "../model/UserBook";

const UserBookSchema = new EntitySchema({
    name: "user_book",
    target: UserBook,
    columns: {
        userBookId: {
            primary: true,
            type: "int",
            generated: true,
        },
        bookId: {
            type: "int"
        },
        username: {
            type: "varchar"
        },
        readPage:{
            type:"int"
        },
        finished:{
            type:"boolean"
        },
        reading:{
            type:"boolean"
        },
    },
    relations: {
        User : {
            target: "my_users",
            type: "many-to-one",
            joinColumn: {
                name: 'username',
              },
            inverseSide:'user_book'
        },
        book : {
            target: "Book",
            type: "many-to-one",
            joinColumn: {
                name: 'bookId',
              },
            inverseSide:'user_book'
        },
    },
    uniques:[
        {
            columns:['bookId','username']
        }
    ]
})

export default UserBookSchema;