import { EntitySchema } from "typeorm";
import {User} from "../model/User";

const UserSchema = new EntitySchema({
    
    name:'User',
    target: User,
    tableName:'my_users',
    columns:{
        username:{
            primary:true,
            type:'varchar',
            unique:true
        },
        name:{
            type: 'varchar',
        },
        email:{
            type:'varchar',
            unique:'true'
        },
        password:{
            type:'varchar',
        },
        createdAt:{
            type:'timestamp without time zone',
            createDate:true,
        },
        updatedAt:{
            type:'timestamp without time zone',
            updateDate:true,
            select:false
        },
    },
    relations:{
        user_book:{
            type:'one-to-many',
            target:'user_book',
            cascade:true,
            inverseSide:'User'
        }
    }
})

export default UserSchema;