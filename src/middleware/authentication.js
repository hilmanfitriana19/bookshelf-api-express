import env from '../config/env';
import { ResponseMessage } from '../utils/constant/response-message.constant';
import { responseUtil } from '../utils/response/response-utils';
import jwt from 'jsonwebtoken';
import { logger } from '../logger/logger.config';

export const JwtAuth = (req,res,next) =>{
    try{
        const auth_header = req.headers['authorization'];
        const token = auth_header.split(' ')[1];
        req.auth = jwt.verify(token,env.JWT_SECRET_KEY);
        next()

    }catch(e){
        logger.error("User not authorized");
        if (e instanceof TypeError && e?.message == `Cannot read properties of undefined (reading 'split')`){
            logger.error("No Token Undefined");
            return responseUtil.UnauthorizedAccess(res,ResponseMessage.NoToken);   
        }else if(e?.message == 'jwt expired'){
            logger.error("JWT Expired");
            return responseUtil.UnauthorizedAccess(res, ResponseMessage.TokenExpired);
        }
        return responseUtil.UnauthorizedAccess(res,ResponseMessage.InvalidToken);
    }
}