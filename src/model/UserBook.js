class UserBook{
    constructor(userBookId, bookId, username, readPage, finished, reading){
        this.userBookId = userBookId;
        this.username=username;
        this.bookId = bookId;
        this.readPage = readPage;
        this.finished = finished;
        this.reading = reading;
    }
}

export default {UserBook : UserBook};