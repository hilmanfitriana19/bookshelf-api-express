import { dataSource } from "../database/database.config";
import BookSchema from "../entity/book-entitiy";

export const bookRepository = dataSource.getRepository(BookSchema);