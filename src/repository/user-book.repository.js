import { dataSource } from "../database/database.config";
import UserBookSchema from "../entity/user-book-entity";

export const userBookRepository = dataSource.getRepository(UserBookSchema);