import { dataSource } from "../database/database.config";
import UserSchema from "../entity/user-entity";

export const userRepository = dataSource.getRepository(UserSchema);