import express from 'express';
import 'express-group-routes';
import { AuthController } from '../controller/auth-controller';
import { BookController } from '../controller/book.controller';
import { UserBookController } from '../controller/user-book.controller';
import { UserController } from '../controller/user.controller';
import { JwtAuth } from '../middleware/authentication';

const routes = express();

routes.group('/book',(router) =>{

    router.post('/', JwtAuth, BookController.create);
    router.get('/', BookController.getAll);
    router.get('/:id', BookController.getOne);
    router.patch('/:id', JwtAuth, BookController.update);
    router.delete('/:id',JwtAuth, BookController.delete);
});

routes.group('/user',(router) =>{

    router.post('/',JwtAuth, UserController.create);
    router.get('/', JwtAuth, UserController.getAll);
    router.get('/detail',JwtAuth, UserController.getAllDetail);
    router.get('/:username', JwtAuth,UserController.getOne);
    router.get('/:username/detail',JwtAuth, UserController.getOneDetail);
    router.patch('/:username', JwtAuth,UserController.update);
    router.delete('/:username', JwtAuth, UserController.delete);
});

routes.group('/user-book',(router) =>{

    router.post('/', JwtAuth,UserBookController.create);
    router.get('/',JwtAuth,UserBookController.getAll);
    router.get('/:userBookId',JwtAuth,UserBookController.getOne);
    router.patch('/:userBookId',JwtAuth,UserBookController.update);
    router.delete('/:userBookId',JwtAuth,UserBookController.delete);
});

routes.group('/auth',(router) =>{

    router.post('/', AuthController.login);
});

export default routes;