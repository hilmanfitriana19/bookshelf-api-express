import app from "./app";
import env from "./config/env";
import * as  db from "./database/database.config";
import { logger } from "./logger/logger.config";

const port = env.PORT;

db.connectDB();
app.listen(port,()=>{
    logger.info(`Running APP in PORT : , ${port}`);
    // console.log("Running APP in PORT : ",port);
})