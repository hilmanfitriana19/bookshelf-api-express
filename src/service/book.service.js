import { bookRepository } from "../repository/book.repository"

export const bookService = ({
    
    getAll : (offset,limit) => {
        return bookRepository.find({
            skip:offset||0,
            take:limit||null
        });
    },
    
    getOne:(id) => {
        return bookRepository.findOneBy({id});
    },

    create: async (body) => {
        const book = await bookRepository.create(body);
        return bookRepository.save(book);
    },

    update: async (id,body) => {
        return bookRepository.update(id,body);
    },

    delete: (id) => {
        return  bookRepository.delete(id);
    }
})