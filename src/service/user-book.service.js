import { userBookRepository } from "../repository/user-book.repository";

export const userBookService = ({
    
    getAll : (offset,limit) => {
        return userBookRepository.find({
            skip:offset||0,
            take:limit||null
        });
    },
    
    getOne:(userBookId) => {
        return userBookRepository.findOneBy({userBookId});
    },

    create: async (body) => {
        const user = userBookRepository.create(body);
        return userBookRepository.save(user);
    },

    update: async (userBookId,body) => {
        return userBookRepository.update(userBookId,body);
    },

    delete: (userBookId) => {
        return  userBookRepository.delete(userBookId);
    }
})