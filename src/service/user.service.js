import { userRepository } from "../repository/user.repository";
import { encodePassword, isCorrectPassword } from "../utils/bcrypt-helper";
import jwt from 'jsonwebtoken';
import env from '../config/env';

export const userService = ({
    
    getAll : (offset,limit) => {

        return userRepository.find({
            select:['username',"email","name"],
            skip:offset||0,
            take:limit||null
        });
    },

    getAllDetail : (offset,limit) => {
        const user = userRepository.createQueryBuilder('user')
        .leftJoinAndSelect('user.user_book','bookUser')
        .leftJoinAndSelect('bookUser.book', 'book')
        .orderBy("user.createdAt")
        .take(limit||null)
        .skip(offset||0)
        .getMany();

        return user;
    },

    getOneDetail : (username) => {
        
        const user = userRepository.createQueryBuilder('user')
        .leftJoinAndSelect('user.user_book','bookUser')
        .leftJoinAndSelect('bookUser.book', 'book')
        .where('user.username = :username',{username : username})
        .getOne();

        return user;
    },
    
    getOne : (username) => {
        return userRepository.findOne({
            where:{
                username,
            },
            select:['username',"email","name"],
        });
    },

    getOneFull : (username) => {
        return userRepository.findOne({
            where:{
                username,
            },
        });
    },

    create: async (body) => {
        const data = await userService.updatePassword(body);
        const user = userRepository.create(data);
        return userRepository.save(user);
    },

    update: async (username,body) => {
        const data = await userService.updatePassword(body);
        return userRepository.update(username,data);
    },

    delete: (username) => {
        return userRepository.delete(username);
    },

    login: async (username,password) =>{
        const user = await userService.getOneFull(username);
        
        if(!user){
            return null;
        }

        if(isCorrectPassword(password,user.password)){
            const payload = {username:username};
            const token = jwt.sign(payload, env.JWT_SECRET_KEY, { expiresIn: env.JWT_EXPIRATION_TIME });
            return {
                jwt_token:token
            };
        }
        return null;
    },

    updatePassword: async (body) =>{
        const newBody = {
          ...body
        }
        newBody['password'] = await encodePassword (body.password);
        
        return newBody;
      }
})