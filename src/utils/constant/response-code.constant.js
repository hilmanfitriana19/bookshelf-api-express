export const ResponseCode = {
    Created : 201,
    Success : 200,

    Fail: 400,
    NotFound: 404,
    Unauthorized: 401,
    
    ServerInternalError:500
}