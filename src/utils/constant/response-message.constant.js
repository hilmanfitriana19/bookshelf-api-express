export const ResponseMessage = {
    SuccessCreated : 'Data has been Created',
    SuccessUpdated : 'Data has been Updated',
    SuccessDeleted : 'Data has been Deleted',
    
    FailCreated: 'Failed to Save Data',
    FailUpdated: 'Failed to Update Data',
    FailDeleted: 'Failed to Delete Data',
    NameBookTaken: 'Failed to Save Data, Book Name Already Register',
    UsernameTaken: 'Failed to Save Data, Username Already Taken',
    EmailTaken: 'Failed to Save Data, Email Already Taken',
    DataNotFound: 'Data Not Found',

    LoginFailed :'Username or Password Not Match',

    TokenExpired:'Token Expired, Please Login Again',
    InvalidToken:'Invalid Token, Please Login Again',
    NoToken:'No Token, Please Login',

    FailProcess: 'Failed to Process Request',
    SuccessProcess: 'Successfully to Process Request'
}