export function UserDetailResponseInterceptor (data){

    function arrangeResponse(element){
        const temp = {
            username: element?.username,
            name: element?.name,
            book :[]
        }
        // loop over userBook
        element?.user_book.forEach(bookData => {
            const book_temp = {
                bookId : bookData?.book?.id,
                name : bookData?.book?.name,
                readPage : bookData?.readPage,
                finished :bookData?.finished,
                reading : bookData?.reading,
            }
            temp.book.push(book_temp);

        })
        return temp;
    }

    if (Array.isArray(data)){
        
        const returned_data = [] ;

        data.forEach(element => {
            returned_data.push(arrangeResponse(element));
        });
        return returned_data;
    }else{
        return arrangeResponse(data);
    }
    
    
}