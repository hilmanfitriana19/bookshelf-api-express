import { ResponseCode } from "../constant/response-code.constant";
import { ResponseMessage } from "../constant/response-message.constant";

export const responseUtil =({
    SuccessCreated : (res) => {
        res.status(ResponseCode.Created).json({"message":ResponseMessage.SuccessCreated});
    },

    SuccessProcess : (res,message) => {
        if (typeof message == 'string' || message==null){
            res.status(ResponseCode.Success).json({"message":message|| ResponseMessage.SuccessProcess});
        } else{
            res.status(ResponseCode.Success).json(message);
        }

    },

    NotFoundData : (res) => {
        res.status(ResponseCode.NotFound).json({"message":ResponseMessage.DataNotFound});
    },

    FailProcess : (res,message) =>{
        res.status(ResponseCode.Fail).json({"message":message|| ResponseMessage.FailProcess});
    },

    UnauthorizedAccess : (res,message) =>{
        res.status(ResponseCode.Unauthorized).json({"message":message|| ResponseMessage.FailProcess});
    },

})
