import Joi from "joi";

export const bookValidator = ({
    create: Joi.object().keys({
        name: Joi.string().required()
        .messages({
            'string.base':'name must be string',
            'string.empty': 'name cannot be empty',
            'any.required': 'name is required'
        }),
        year: Joi.number().optional().integer().min(1000)
        .messages({
            'number.base' : 'year must be number',
            'number.min': 'year must be greater than or equal to {#limit}'

        }),
        author: Joi.string().required().allow('')
        .messages({
            'string.base':'author must be string',
            'any.required': 'author is required'
        }),
        summary: Joi.string().optional().allow('')
        .messages({    
            'string.base':'summary must be string',
        }),
        publisher: Joi.string().optional().allow('')
        .messages({    
            'string.base':'publisher must be string',
        }),
        pageCount: Joi.number().optional()
        .messages({
            'number.base' : 'pageCount must be number'
        }),
        
    }),
    update: Joi.object().keys({
        name: Joi.string().optional()
        .messages({
            'string.base':'name must be string',
            'string.empty': 'name cannot be empty',
            'any.required': 'name is required'
        }),
        year: Joi.number().optional().integer().min(1000)
        .messages({
            'number.base' : 'year must be number',
            'number.min': 'year must be greater than or equal to {#limit}'
        }),
        author: Joi.string().optional()
        .messages({
            'string.base':'name should be string'
        }),
        summary: Joi.string().optional().messages({
            'string.base':'summary must be string',
        }),
        publisher: Joi.string().optional()
        .messages({    
            'string.base':'publisher must be string',
        }),
        pageCount: Joi.number().optional()
        .messages({
            'number.base' : 'pageCount must be number'
        }),
        
    }),
})