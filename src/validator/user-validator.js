import Joi from "joi";

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/


export const userValidator = ({
    create: Joi.object().keys({
        username: Joi.string().required()
        .messages({
            'string.base':'username must be string',
            'string.empty': 'username cannot be empty',
            'any.required': 'username is required'
        }),
        name: Joi.string().required()
        .messages({
            'string.base':'name must be string',
            'string.empty': 'name cannot be empty',
            'any.required': 'name is required'
        }),
        email: Joi.string().required()
        .pattern(emailRegex)
        .messages({
            'string.base':'email must be string',
            'string.empty': 'email cannot be an empty',
            'string.pattern.base': 'email is invalid',
            'any.required': 'email is required'
        }),
        password: Joi.string().required()
        .pattern(passwordRegex)
        .messages({
            'string.base':'password should be string',
            'string.empty': 'password cannot be an empty',
            'string.min': 'password should have a minimum length of {#limit}',
            'string.pattern.base': 'password must include minimum eight characters, at least one letter and one number',
            'any.required': 'password is required'
        })
    }),
    update: Joi.object().keys({
        username: Joi.string().optional()
        .messages({
            'string.base':'username should be string',
            'string.empty': 'username cannot be empty'
        }),
        name: Joi.string().optional()
        .messages({
            'string.base':'name should be string',
            'string.empty': 'name cannot be empty'
        }),
        email: Joi.string()
        .pattern(emailRegex)
        .messages({
            'string.base':'email should be string',
            'string.empty': 'email cannot be an empty',
            'string.pattern.base': 'email is invalid'
        }),
        password: Joi.string()
        .pattern(passwordRegex)
        .messages({
            'string.empty': 'password cannot be an empty',
            'string.min': 'password should have a minimum length of {#limit}',
            'string.pattern.base': 'password must include minimum eight characters, at least one letter and one number',
        })
    }),
    login: Joi.object().keys({
        username: Joi.string().required()
        .messages({
            'string.base':'username should be string',
            'string.empty': 'username cannot be empty',
            'any.required': 'username is required'
        }),
        password: Joi.string().required()
        .messages({
            'string.base':'passowrd should be string',
            'string.empty': 'password cannot be an empty',
            'any.required': 'password is required'
        })
    }),
})